#pragma once

#ifndef _WIESZCHOLEK_  
#define _WIESZCHOLEK_


template<typename E>
class wieszcholek
{

private:
	int iterator;
	E wartosc;
public:
	wieszcholek(E x = 1,int i =0) : wartosc(x), iterator(i)
	{}

	E get_wartosc()
	{
		return wartosc;
	}

	void set_wartosc(E x)
	{
		wartosc=x;
	}

	int get_iterator()
	{
		return iterator;
	}

	void set_iterator(int x)
	{
		iterator = x;
	}

};

#endif 