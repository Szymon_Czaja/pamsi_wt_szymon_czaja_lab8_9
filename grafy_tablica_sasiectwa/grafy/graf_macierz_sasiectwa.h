#pragma once

#ifndef _GRAF_MACIERZ_SASIECTWA_  
#define _GRAF_MACIERZ_SASIECTWA_  

#include"lista.h"
#include"krawedz.h"
#include <ctime>

template<typename E>
class graf_macierz_sasiectwa
{
private:
	Deque<wieszcholek<E>*> wieszcholki;
	Deque<krawedz<E>*> krawedzie;
	krawedz<E>*** tab;
	int iter;
public:
	graf_macierz_sasiectwa()
	{
		iter = 0;
	}

	~graf_macierz_sasiectwa()
	{
		;//	while (!wieszcholki.empty())
		//	removeVertex(wieszcholki[0]);
	}

	int get_count_Vertex()
	{
		return wieszcholki.get_size();
	}

	int get_count_Edge()
	{
		return krawedzie.get_size();
	}

	int get_iter()
	{
		return iter;
	}

	wieszcholek<E>* get_vertex(int i)
	{
		return wieszcholki[i];
	}

	krawedz<E>* get_edge(int i)
	{
		return krawedzie[i];
	}

//===========================METODY DOSTEPU===========================================

	wieszcholek<E>** endVertices(krawedz<E>*ele)
	{
		return ele->get_tab();
	}

	wieszcholek<E>* opposite(wieszcholek<E>* v, krawedz<E>* e)
	{
		if (e->get_jeden() == v)
			return e->get_drugi();
		else if (e->get_drugi() == v)
			return e->get_jeden();
		else
			return nullptr;
	}

	bool areAdjacent(wieszcholek<E>* v, wieszcholek<E>* w)
	{
		if(tab[v->get_iterator()][w->get_iterator()]!=nullptr)
			return true;
		return false;
	}

	void replace(wieszcholek<E>* v, E ele)
	{
		v->set_wartosc(ele);
	}

	void replace(krawedz<E>* e, E ele)
	{
		e->set_wartosc(ele);
	}

//===========================METODY UAKTUALNIAJACE===========================================

	void insertVertex(E dana)
	{
		
		krawedz<E>*** tmp =tab;
		tab = new krawedz<E>**[iter + 1];
		for (int i = 0;i < iter + 1;i++)
			tab[i] = new krawedz<E>*[iter + 1];

		for (int i = 0;i <= iter;i++)
			for (int j = 0;j <= iter;j++)
			{
				if (i != iter && j != iter)
					tab[j][i] = tmp[j][i];
				else
					tab[j][i] = nullptr;
			}
		wieszcholki.add_back(new wieszcholek<E> (dana, iter));
		for (int i = 0;i < iter;i++)
			delete[] tmp[i];
		delete[] tmp;
		iter = wieszcholki.get_size();
	}

	void insertVertex(wieszcholek<E>* v)
	{

		krawedz<E>*** tmp = tab;
		tab = new krawedz<E>**[iter + 1];
		for (int i = 0;i < iter + 1;i++)
			tab[i] = new krawedz<E>*[iter + 1];

		for (int i = 0;i <= iter;i++)
			for (int j = 0;j <= iter;j++)
			{
				if (i != iter && j != iter)
					tab[j][i] = tmp[j][i];
				else
					tab[j][i] = nullptr;
			}
		wieszcholki.add_back(v);

		for (int i = 0;i < iter;i++)
			delete [] tmp[i] ;
		delete[] tmp;
		iter = wieszcholki.get_size();
	}


	void insertEdge(wieszcholek<E> *a, wieszcholek<E> *b, E dana=1)
	{
		krawedzie.add_back(new krawedz<E> (a, b, dana));
		tab[wieszcholki.index_of(a)][wieszcholki.index_of(b)] = krawedzie[krawedzie.get_size()-1];
		tab[wieszcholki.index_of(b)][wieszcholki.index_of(a)] = krawedzie[krawedzie.get_size() - 1];
	}

	void insertEdge(krawedz<E>*k)
	{
		krawedzie.add_back(k);
		tab[wieszcholki.index_of(k->get_jeden())][wieszcholki.index_of(k->get_drugi())] = krawedzie[krawedzie.get_size() - 1];
		tab[wieszcholki.index_of(k->get_drugi())][wieszcholki.index_of(k->get_jeden())] = krawedzie[krawedzie.get_size() - 1];
	}

	void removeVertex(wieszcholek<E>* ele)
	{
		for(int i=0;i<iter;i++)
			removeEdge(tab[ele->get_iterator()][i]);

		krawedz<E>*** tmp = tab;
		tab = new krawedz<E>**[iter - 1];
		for (int i = 0;i < iter - 1;i++)
			tab[i] = new krawedz<E>*[iter - 1];
		
		for (int i = 0;i < iter-1;i++)
			for (int j = 0;j < iter-1;j++)
			{
				if (i < ele->get_iterator()&&j<ele->get_iterator())
					tab[j][i] = tmp[j][i];
				else if(i >= ele->get_iterator() && j<ele->get_iterator())
					tab[j][i]=tmp[j][i+1];
				else if (i < ele->get_iterator() && j>=ele->get_iterator())
					tab[j][i] = tmp[j+1][i];
				else
					tab[j][i] = tmp[j + 1][i+1];
			}

		for (int i = wieszcholki.index_of(ele);i < wieszcholki.get_size();i++)
			wieszcholki[i]->set_iterator(wieszcholki[i]->get_iterator() - 1);

		wieszcholki.del(wieszcholki.index_of(ele));
		delete ele;
		for (int i = 0;i < iter;i++)
			delete[] tmp[i];
		delete[] tmp;
		iter = wieszcholki.get_size();
	}

	void removeEdge(krawedz<E>* ele)
	{
		if (ele == nullptr)
			return;
	
		tab[ele->get_jeden()->get_iterator()][ele->get_drugi()->get_iterator()] = nullptr;
		tab[ele->get_drugi()->get_iterator()][ele->get_jeden()->get_iterator()] = nullptr;
		krawedzie.del(krawedzie.index_of(ele));
		delete ele;
	}

//===========================METODY ITERUJACE===========================================
	
	Deque<krawedz<E>*> incidentEdges(wieszcholek<E> *v)
	{
		Deque<krawedz<E>*> ret;
		for(int i=0;i<wieszcholki.get_size();i++)
			if (tab[v->get_iterator()][i] != nullptr)
				 ret.add_back(tab[v->get_iterator()][i]);
		return ret;
	}

	Deque<wieszcholek<E>*>* vertices()
	{
		return &wieszcholki;
	}

	Deque<krawedz<E>*>* edges()
	{
		return &krawedzie;
	}
	
	//=====================METODY LISTA 7 I 8========================================

	void generuj( int wieszcholki, float gestosc)
	{
		if (gestosc<0 || gestosc>1)
		{
			string wyjatek = " generuj() zla gestosc !";
			throw wyjatek;
		}
		while (!vertices()->empty())
				removeVertex(vertices()->operator[](0));

		int krawedzie = (wieszcholki*(wieszcholki - 1)*gestosc) / 2;
		srand(time(NULL));
		for (int i = 0;i < wieszcholki;i++)
			insertVertex(rand() % 10000);

		wieszcholek<E>*ptr;
		wieszcholek<E>*ptr2;
		for (int i = 0;i < krawedzie;i++)
		{
			ptr = get_vertex(rand() % wieszcholki);
			ptr2 = get_vertex(rand() % wieszcholki);
			if (areAdjacent(ptr, ptr2) || ptr == ptr2)
				i--;
			else
				insertEdge(ptr, ptr2, rand() % 10000);
		}
	}


};

#endif 