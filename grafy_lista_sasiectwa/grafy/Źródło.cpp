
#include"graf_lista_sasiectwa.h"
#include "kolejka_priorytetowa.h"


template<typename E>
graf_lista_sasiectwa<E> kruskal(graf_lista_sasiectwa<E> &graf)							//wejscie- graf zwraca minimalne drzewp rozpinajace
{
	Deque_p <krawedz<E>*> kraw;																   //kolej_p wszystkich krawedzi
	Deque <krawedz<E>*> kr;																		//kolej krawedzi potrzebnych do drzewa
	Deque<Deque<wieszcholek<E,krawedz<E>>*>*>wiesz;														//kolejka kast
	for (int i = 0;i < graf.get_count_Vertex();i++)												//przepisanie kazdego wieszcholka do osobnej kasty
	{
		wiesz.add_back(new Deque<wieszcholek<E, krawedz<E>>*>);
		wiesz[wiesz.get_size() - 1]->add_back(graf.vertices()->operator[](i));
	}
	wezel<krawedz<E>*>* tmp= graf.edges()->get_head();
	for (int i = 0;i < graf.get_count_Edge();i++) {
		kraw.insert(tmp->get_element()->get_wartosc(), tmp->get_element());	 //przepisanie wszystkich krawedzi do kolejki pryriorytetowrj
		tmp = tmp->get_next();
	}
	graf_lista_sasiectwa<E> drzewo;															//drzewo ktore bedziemy zwracac
	krawedz<E>*k;																				//pojedyncza wylosoana krawedz o najmniejszej wadze
	while (!kraw.empty())																		//dopuki sie nie skoncza krawedzie
	{
		k = kraw.remove_min();																	//losujemy najmniejsza krawedz
		for (int i = 0;i < wiesz.get_size();i++)												//dla wszystkich kast
		{
			if (wiesz[i]->jest(k->get_jeden()) && wiesz[i]->jest(k->get_drugi()))				//jezeli kraw->1 i kraw->2 sa w tej samej kascie
			{
				if (kraw.get_kol()->get_size() > 0)												//spr czy krawedz nie jest ostatnia krawedzia bo nie bylo by skad juz zabrac nowych krawedzi
				{
					k = kraw.remove_min();														//pobiera najmniejsza nowa krawedz
					i = -1;																		//ustawia nowy iterator na -1 zeby for zaczal sie od nowa dla danej krawedzi
				}
			}
			else if (wiesz[i]->jest(k->get_jeden()))											//jezeli jest kraw->1 
			{
				for (int j = 0;j < wiesz.get_size();j++)										//szukamy kraw->2 
					if (wiesz[j]->jest(k->get_drugi()))
					{
						wiesz[i]->doczep(wiesz[j]);												//laczymy kasty
						wiesz.del(j);															//usuwamy stara kaste
						kr.add_back(k);															//dodajemy do listy krawedzi ktora bedzie zwrocona w drzewie rozpinajacym
						j = wiesz.get_size();
					}
			}
			else if (wiesz[i]->jest(k->get_drugi()))											//to samo jezeli najpierw znajdzie sie kraw->2
			{
				for (int j = 0;j < wiesz.get_size();j++)
					if (wiesz[j]->jest(k->get_jeden()))
					{
						wiesz[i]->doczep(wiesz[j]);
						wiesz.del(j);
						kr.add_back(k);
						j = wiesz.get_size();
					}
			}
			if (wiesz.get_size() == 1 && wiesz[0]->get_size() == graf.get_count_Vertex()) //jesli jest jedna kasta i zawiera wszystkie wieszkolki
			{
				for (int j = 0;j < graf.get_count_Vertex();j++)								//dodaj ja do drzewa 
					drzewo.insertVertex(wiesz[0]->operator[](j));
				for (int j = 0;j < kr.get_size();j++)
					drzewo.insertEdge(kr[j]);												//dodawj wylosowane wczesniej krawedzie potrzebne
				return drzewo;																//zwroc drzewo
			}
		}
	}
	return drzewo;																			//zwraca zdrzewo z 0 wieszcholkow i 0 krawedzi poniewaz drzewo nie bylo spojne
}

template<typename E>
graf_lista_sasiectwa<E> prime(graf_lista_sasiectwa<E> &graf)//na wejsciu graf, na wyjsciu minimalne drzewo rozpinajace
{
	bool *visited = new bool[graf.get_count_Vertex()];			// tablicy odwiedzi 
	Deque_p<krawedz<E>*> lista_krawedzi;						//kolejka przyriorytetowa krawedzi z ktorej bedziemy losowac
	graf_lista_sasiectwa<E> drzewo;							//drzewo ktore zwrocimy
	Deque<krawedz<E>*>* krawedzie_przylegle;						//kolejka przyleglych krawedzi
	krawedz<E>* najmniejsza_krawedz;							//zmienna pomocnicza - krawedz o najnizszej wadze

	for (int i = 0;i < graf.get_count_Vertex();i++)				//dodawnie do drzewa wszystkich wieszcholkow
		drzewo.insertVertex(graf.get_vertex(i));
	for (int i = 0;i<graf.get_count_Vertex();i++)					//ustawienie wszystkich odwieszin na false
		visited[i] = false;
	visited[0] = true;											//po za jednym
	int j = 0;													//pomocniczy index rozpatrywanego wieszcholka
	while (drzewo.get_count_Edge() != graf.get_count_Vertex() - 1)//dopuki drzewo nie bedzie zawieralo krawedzi = liczbie wieszcholkow -1
	{
		wezel<krawedz<E>*>* tmp;
		krawedzie_przylegle = graf.incidentEdges(graf.get_vertex(j));//pobranie przyleglych krawedzi pierwszego wieszcholka
		tmp = krawedzie_przylegle->get_head();
		for (int i = 0;i < krawedzie_przylegle->get_size();i++)		//dla wszystkich tych krawedzi
		{
			if (!visited[graf.vertices()->index_of(graf.opposite(graf.get_vertex(j), tmp->get_element()))])//jesli wieszcholek do ktrego prowadzi i-ta krawedz nie byl juz odwiedzany
				lista_krawedzi.insert(tmp->get_element()->get_wartosc(), tmp->get_element());//dodaaj do kolejki pryriorytetowej i-ta krawedz
			tmp = tmp->get_next();
		}
		najmniejsza_krawedz = lista_krawedzi.remove_min();			//pobierz najmniejsza krawedz
		while (visited[graf.vertices()->index_of(najmniejsza_krawedz->get_jeden())] && visited[graf.vertices()->index_of(najmniejsza_krawedz->get_drugi())]) //jezeli wieszcholek do ktorego prowadzi krawedz byl odwiedzaany
			najmniejsza_krawedz = lista_krawedzi.remove_min();		//dalej pobieraj krawedzie

		drzewo.insertEdge(najmniejsza_krawedz);					//jak znajdziesz dodaj krawedz do drzewa

		if (visited[graf.vertices()->index_of(najmniejsza_krawedz->get_jeden())])	//jesli "nowy" wieszcholek nie byl w krawedz->jeden
		{
			j = graf.vertices()->index_of(najmniejsza_krawedz->get_drugi());		//zamien indeks rozpatrywanego wieszchilka
			visited[graf.vertices()->index_of(najmniejsza_krawedz->get_drugi())] = true;//ustaw ze byl juz odiedzony
		}
		else															//to samo co wyzej tylko dla krawedz->drugi
		{
			j = graf.vertices()->index_of(najmniejsza_krawedz->get_jeden());
			visited[graf.vertices()->index_of(najmniejsza_krawedz->get_jeden())] = true;
		}
	}
	delete[] visited;													//usun tablice odwiedzin
	return drzewo;														//zwroc minimalne drzewo rozpinajace
}

template<typename E>
int **dijkstry(graf_lista_sasiectwa<E> &graf, wieszcholek<E,krawedz<E>>*start) //zwraca tablice odleglosci do V start i kolejnosc odwiedzanych sasiadow wg indeksow
{
	bool* rozpatrywane = new bool[graf.get_count_Vertex()];		//tab tablica rozpatrzonych wieszcholkow
	Deque_p<int> dystans;										//index wieszcholka jako element
	Deque<krawedz<E>*>* krawedzie;								//lista krawedzi wychodzoncych z danego wieszcholka
	int **tab = new int*[graf.get_count_Vertex()];				//tab 2 wym - wynik 
	int najmniejszy;											//zmienna pomocnicza - indeks wieszcholka z najmniejsza droga 
	int next;													//zmienna pomocnicza - indeks wieszcholka nastepnego
	for (int i = 0;i < graf.get_count_Vertex();i++)
		tab[i] = new int[2];
	for (int i = 0;i < graf.get_count_Vertex();i++)				//inicjowanie wartosci poczatkowych
	{
		tab[i][0] = 999999;
		tab[i][1] = -1;
		rozpatrywane[i] = true;
		dystans.insert(tab[i][0], i);
	}
	dystans.replace_key(0, graf.vertices()->index_of(start));				//zmiana pierewszego wieszcholka na odleglosc 0
	tab[graf.vertices()->index_of(start)][0] = 0;

	for (int i = 0;i < graf.get_count_Vertex();i++)				//petla glowna 
	{
		najmniejszy = dystans.remove_min();						//odszukanie wieszcholka o najniejszym koscie przejscia
		rozpatrywane[najmniejszy] = false;						//ustawienie go jako juz odwiedzanego i pomijanie w nastepnych iteracjacch
		krawedzie = graf.incidentEdges(graf.get_vertex(najmniejszy));//pobranie listy krawedzi sasiednich do niego
		for (int k = 0;k < krawedzie->get_size();k++)			//dla wszystkich krawedzi tego wieszhcolka
		{
			next = graf.vertices()->index_of(graf.opposite(graf.get_vertex(najmniejszy), krawedzie->operator[](k)));//pobieranie wieszcholka na przeciwko najmniejszego wzgledem k-tej krawedzi
			if (rozpatrywane[next])								//spr czy nie byl juz rozpatrywany
				if (tab[next][0] > tab[najmniejszy][0] + krawedzie->operator[](k)->get_wartosc())	//jesli odleglosc kta tam jest jest wieksza od odleglosci przez najmniejszy + krawedz do nastepnego
				{
					tab[next][0] = tab[najmniejszy][0] + krawedzie->operator[](k)->get_wartosc();	//zamien odleglosci (relaksacja krawedzi)
					dystans.replace_key(tab[next][0] + krawedzie->operator[](k)->get_wartosc(), next);//uaktualni klucz
					tab[next][1] = najmniejszy;											//ustaw poprzednika
				}
		}
	}

	return tab;														//zwraca tablele z droga i odleglosciami

}


int main()
{
	graf_lista_sasiectwa<int> g;
	graf_lista_sasiectwa<int> d1;
	clock_t start, stop;
	double krusk[100];
	double dikstr[100];
	double prim[100];
	double srednia;
	int **del;

	int n[5] = { 10,50,100,500,1000 };
	double proc[4] = { 0.25,0.5,0.75,1 };

	for (int l = 0;l < 5;l++)
	{
		for (int k = 0;k < 4;k++)
		{
			cout << endl << "sredni czas algorytmu dla grafow zawierajacych " << n[l] << " wieszcholkow. " << endl << "wyciagniety z 100 prob" << endl << "gestosc grafu: " << proc[k] << endl;

			for (int j = 0;j < 100; j++)
			{
				if (j % 5 == 0)
					cout << j << "  ";
				d1.vertices()->set_size(0);
				while (d1.get_count_Vertex() == 0)
				{
					g.generuj(n[l], proc[k]);
					d1 = kruskal(g);
				}


				start = clock();
				kruskal(g);
				stop = clock();
				krusk[j] = (double)(stop - start) / CLOCKS_PER_SEC;

				start = clock();
				prime(g);
				stop = clock();
				prim[j] = (double)(stop - start) / CLOCKS_PER_SEC;

				start = clock();
				del=dijkstry(g, g.get_vertex(0));
				stop = clock();
				dikstr[j] = (double)(stop - start) / CLOCKS_PER_SEC;
				
				for (int i = 0;i < g.get_count_Vertex();i++)
					delete[] del[i];
				delete[] del;
			}

			srednia = 0;
			for (int i = 0;i < 100;i++)
				srednia = srednia + prim[i];
			cout << "algorytm prima srednia z 100 pomiarow: " << srednia / 100 << endl;

			srednia = 0;
			for (int i = 0;i < 100;i++)
				srednia = srednia + krusk[i];
			cout << "algorytm kruskowa srednia z 100 pomiarow: " << srednia / 100 << endl;

			srednia = 0;
			for (int i = 0;i < 100;i++)
				srednia = srednia + dikstr[i];
			cout << "algorytm dijkstry srednia z 100 pomiarow: " << srednia / 100 << endl;
		}
	}
	system("PAUSE");
	return 0;
}