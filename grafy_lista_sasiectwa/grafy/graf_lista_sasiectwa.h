#pragma once

#ifndef _GRAF_LISTA_SASIECTWA_  
#define _GRAF_LISTA_SASIECTWA_  
#include <ctime>
#include"krawedz.h"

template<typename E>
class graf_lista_sasiectwa
{
private:
	Deque<wieszcholek<E, krawedz<E>>*> wieszcholki;
	Deque<krawedz<E>*> krawedzie;

public:
	graf_lista_sasiectwa() = default;

	~graf_lista_sasiectwa()
	{
		;//while (!wieszcholki.empty())
			//removeVertex(wieszcholki[0]);
	}

	int get_count_Vertex()
	{
		return wieszcholki.get_size();
	}

	int get_count_Edge()
	{
		return krawedzie.get_size();
	}

	wieszcholek<E, krawedz<E>>* get_vertex(int i)
	{
		return wieszcholki[i];
	}

	krawedz<E>* get_edge(int i)
	{
		return krawedzie[i];
	}

//===========================METODY DOSTEPU===========================================

	wieszcholek<E, krawedz<E>>** endVertices(krawedz<E>*ele)
	{
		return ele->get_tab();
	}

	wieszcholek<E, krawedz<E>>* opposite(wieszcholek<E, krawedz<E>>* v, krawedz<E>* e)
	{
		if (e->get_jeden() == v)
			return e->get_drugi();
		else if (e->get_drugi() == v)
			return e->get_jeden();
		else
			return nullptr;
	}

	bool areAdjacent(wieszcholek<E, krawedz<E>>* v, wieszcholek<E, krawedz<E>>* w)
	{
		for (int i = 0;i < v->get_sasiectwo()->get_size();i++)
			if (opposite(v, v->get_sasiectwo()->operator[](i)) == w)
				return true;
		return false;
	}

	void replace(wieszcholek<E, krawedz<E>>* v, E ele)
	{
		v->set_wartosc(ele);
	}

	void replace(krawedz<E>* e, E ele)
	{
		e->set_wartosc(ele);
	}

//===========================METODY UAKTUALNIAJACE===========================================

	void insertVertex(E dana)
	{
		wieszcholki.add_back(new wieszcholek<E, krawedz<E>> (dana));
	}
	void insertVertex(wieszcholek<E, krawedz<E>>* dana)
	{
		wieszcholki.add_back(dana);
	}

	void insertEdge(wieszcholek<E,krawedz<E>> *a, wieszcholek<E,krawedz<E>> *b, E dana=1)
	{
		krawedzie.add_back(new krawedz<E> (a, b, dana));
		a->add_krawedz(krawedzie[krawedzie.get_size()-1]);
		b->add_krawedz(krawedzie[krawedzie.get_size() - 1]);
	}
	void insertEdge( krawedz<E>* dana)
	{
		krawedzie.add_back(dana);

	}

	void removeVertex(wieszcholek<E, krawedz<E>>* ele)
	{
		while(!ele->get_sasiectwo()->empty())
			removeEdge(ele->get_krawedz(0));
		wieszcholki.del(wieszcholki.index_of(ele));
		delete ele;
	}


	void removeEdge(krawedz<E>* ele)
	{
		ele->get_jeden()->get_sasiectwo()->del(ele->get_jeden()->get_sasiectwo()->index_of(ele));
		ele->get_drugi()->get_sasiectwo()->del(ele->get_drugi()->get_sasiectwo()->index_of(ele));
		krawedzie.del(krawedzie.index_of(ele));
		delete ele;
	}

//===========================METODY ITERUJACE===========================================

	Deque<krawedz<E>*>* incidentEdges(wieszcholek<E, krawedz<E>> *v)
	{
		return v->get_sasiectwo();
	}

	Deque<wieszcholek<E, krawedz<E>>*>* vertices()
	{
		return &wieszcholki;
	}

	Deque<krawedz<E>*>* edges()
	{
		return &krawedzie;
	}

	//=====================METODY LISTA 7 I 8========================================

	void generuj(int wieszcholki, float gestosc)
	{
		if (gestosc<0 || gestosc>1)
		{
			string wyjatek = " generuj() zla gestosc !";
			throw wyjatek;
		}
		while (!vertices()->empty())
			removeVertex(vertices()->operator[](0));
		int krawedzie = (wieszcholki*(wieszcholki - 1)*gestosc) / 2;
		srand(time(NULL));
		for (int i = 0;i < wieszcholki;i++)
			insertVertex(rand() % 10000);

		wieszcholek<E,krawedz<E>>*ptr;
		wieszcholek<E, krawedz<E>>*ptr2;
		for (int i = 0;i < krawedzie;i++)
		{
			ptr = get_vertex(rand() % wieszcholki);
			ptr2 = get_vertex(rand() % wieszcholki);
			if (areAdjacent(ptr, ptr2) || ptr == ptr2)
				i--;
			else
				insertEdge(ptr, ptr2, rand() % 10000);
		}
	}
};

#endif 